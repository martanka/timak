# Ako rozbehat TIMAK

## BackEnd

1. nainstalovat Javu 1.8 a nastavit ju v IntelliJ Idea ako SDK
2. nainstalovat PostgreSQL
3. psql console:
    ```
    CREATE USER invest WITH PASSWORD 'P6x7u7naTKpRk4UP';
    CREATE DATABASE invest WITH OWNER invest;
    GRANT ALL ON DATABASE invest TO invest;
    ```
4. nastartuj backend a postupne spusti loadre z precinku ./common/loaders
5. InfluxDB ---- R.I.P. -- mozes si to nainstalovat ale pokial nemas na pocitaci este niekde seed od Slava tak nic s tym nespravis

## FrontEnd
1. vo folderi /client sprav `npm install`
2. zmen proxy.conf.json podla README.md co tam je
3. `npm run start`
4. LOGIN: test@test.com
        12345678